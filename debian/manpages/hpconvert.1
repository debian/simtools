.\" hpconvert.1
.\"
.\" Copyright 2017, Andreas Bombe
.\"           2012, J. David Bryan
.\"
.\" Permission is hereby granted, free of charge, to any person obtaining a copy
.\" of this software and associated documentation files (the "Software"), to
.\" deal in the Software without restriction, including without limitation the
.\" rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
.\" sell copies of the Software, and to permit persons to whom the Software is
.\" furnished to do so, subject to the following conditions:
.\"
.\" The above copyright notice and this permission notice shall be included in
.\" all copies or substantial portions of the Software.
.\"
.\" THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
.\" IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
.\" FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
.\" AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
.\" LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
.\" FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
.\" IN THE SOFTWARE.
.\"
.\" Except as contained in this notice, the name(s) of the above copyright
.\" holders shall not be used in advertising or otherwise to promote the sale,
.\" use or other dealings in this Software without prior written authorization.
.\"
.\" ----------------------------------------------------------------------------
.TH HPCONVERT 1 2017\-10\-15 "simtools"
.SH NAME
hpconvert \- convert HP disk image between SIMH and HPDrive formats
.\" ----------------------------------------------------------------------------
.SH SYNOPSIS
\fBhpconvert\fR \fIFILE\fR
.\" ----------------------------------------------------------------------------
.SH DESCRIPTION
.UR http://www.hp9845.net/9845/projects/hpdrive/
HPDrive
.UE
is a free program written by Ansgar Kueckes that uses a PC and a GPIB card to
emulate a variety of vintage Hewlett-Packard disc and tape drives that interface
to computers via the HP-IB.
.PP
\fBhpconvert\fR converts an HP disc image from SIMH to HPDrive format or
vice-versa.
This permits interchanging images between the two programs.
.PP
SIMH writes and reads disc images in little-endian format, regardless of the
byte order of the host.
In addition, SIMH accesses images for the the 7905 and 7906 drives in platter
order (i.e., all tracks on heads 0 and 1, followed by all tracks on heads 2 and
3) to improve locality of access.
It accesses images for the 7920, 7925, and all CS/80 drives in cylinder order.
.PP
HPDrive writes and reads images in big-endian format, regardless of the byte
order of the host, and accesses all images in cylinder order.
.PP
This program swaps each pair of bytes in the disc image.
In addition, if the image is precisely the size of a 7905 or 7906 drive
(15,151,104 or 20,201,472 bytes, respectively), the access order is restructured
from platter to cylinder, or vice-versa.
.PP
Note that SIMH does not currently create full-size disc images unless the last
sector on the drive is written.
Therefore, it is recommended that new images be initialized using the RTE FORMT
or SWTCH programs to ensure that the image files are of the correct size for
this program and HPDrive.
.PP
This program creates a scratch file to store the converted image.
Only when conversion is complete is the original file deleted and the scratch
file renamed to the original file's name.
Running the program twice on the same file will return the file to its original
configuration.
.PP
To decide the mode used in a 7905 or 7906 image, the program examines the OS
signature at the start of the file and compares it against a list of known
operating systems.
If the OS signature is not recognized, the program terminates without altering
the file.
Signature checking is not performed on images other than those for the 7905 and
7906; those images are byte-swapped unconditionally.
.PP
Signatures for these operating systems and compatible 7905/06 drives are
recognized:
.IP \(em 2
HP 1000 RTE-IVB:  7906H ICD
.IP \(em 2
HP 1000 RTE-6/VM: 7906H ICD
.IP \(em 2
HP 3000 MPE:      7905A/7906A MAC
.PP
The signatures are contained in the first four words at the start of each disc
image.
In hex representation, they are:
.IP \(em 2
RTE-IVB  ICD: 676D 06C0 776B 0B40 (LDA HHIGH ; CMA,CCE ; STA HRCNT ; ERB)
.IP \(em 2
RTE-6/VM ICD: 676D 06C0 776B 0B40 (LDA HHIGH ; CMA,CCE ; STA HRCNT ; ERB)
.IP \(em 2
MPE MAC:      5359 5354 454D 2044 ("SYSTEM D")
.PP
These represent the start of the boot extension for RTE and the start of the
system disc label for MPE.
The boot extension machine instructions are:
.SS RTE-IVB ICD
.in +4n
.EX
62000         LDA   EQU 062000B
72000         STA   EQU 072000B
01200         O0    EQU HSTRT-1400B

02600 063555  HSTRT ABS LDA+HHIGH-O0   (word 1)
02601 003300            CMA,CCE        (word 2)
02602 073553        ABS STA+HRCNT-O0   (word 3)
02603 005500            ERB            (word 4)

02753 000000  HRCNT NOP

02755 077377  NW#DS OCT 77377
02755         HHIGH EQU NW#DS
.EE
.in
.SS RTE-6/VM ICD
.in +4n
.EX
      062000  LDA   EQU 062000B
      072000  STA   EQU 072000B
      000000R O0    EQU HSTRT-1400B

01400 063555  HSTRT ABS LDA+HHIGH-O0   (word 1)
01401 003300            CMA,CCE        (word 2)
01402 073553        ABS STA+HRCNT-O0   (word 3)
01403 005500            ERB            (word 4)

01553 000000  HRCNT NOP

01555 077377  NW#DS OCT 77377
      001555R HHIGH EQU NW#DS
.EE
.in
.PP
And the disc label is:
.SS MPE MAC and CS/80
.in +4n
.EX
00000 051531  LABEL ASC 6,SYSTEM DISC  (word 1)
00001 051524                           (word 2)
00002 042515                           (word 3)
00003 020104                           (word 4)
00004 044523
00005 041440
.EE
.in
